package com.pipongteam.admob;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdRequest;

public class AdmobUtils {

    private static AdmobUtils instance;
    private static MasterApplication mApplication;

    private AdmobUtils(MasterApplication application) {
        mApplication = application;
    }

    public static synchronized AdmobUtils getInstance(MasterApplication application) {
        if (instance == null) {
            instance = new AdmobUtils(application);
        }
        return instance;
    }

    public static void showInterstitialAd() {
        mApplication.showInterstitialAd();
    }

    public static boolean isLoadInterstitial() {
        return mApplication.isLoadInterstitial();
    }

    public static void initBannerAd(View view, int layout_banner_id) {
        mApplication.initBannerAd(view, layout_banner_id);
    }
}
