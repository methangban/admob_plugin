package com.pipongteam.admob;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MasterApplication extends Application {

    private static final String TAG = MasterApplication.class.getName();
    private InterstitialAd mInterstitialAd;
    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate MasterApplication");
        super.onCreate();
        // Init admod
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });

        initAdmobSetting();
    }

    private void initAdmobSetting() {
        mInterstitialAd = new InterstitialAd(MasterApplication.this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitialad_id));
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        adRequestBuilder.addTestDevice("848600765890430B2ADE473769E3599F");
        mInterstitialAd.loadAd(adRequestBuilder.build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });
    }


    public void showInterstitialAd() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    public boolean isLoadInterstitial() {
        return mInterstitialAd.isLoaded();
    }

    public void initBannerAd(View view, int layout_banner_id){
        AdView mAdView = view.findViewById(layout_banner_id);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}